import numpy as np
import openrouteservice
from openrouteservice import convert
import json

leit = np.zeros((1,2))
leit[0] = (8.6787, 49.980002)

def get_einsatzort (leitstellen):
    rand = np.random.randint(0, leitstellen.shape[0])

    coord = leitstellen[rand]

    x0 = coord[0]
    y0 = coord[1]

    alp = (np.random.rand(1)*(np.pi*2))[0]

    r=0.35

    x1=x0+np.cos(alp)*r
    y1=y0+np.sin(alp)*r

    return np.array([[x0, y0], [x1, y1]])

def get_routes ():
    coords = get_einsatzort(leit)

    zero = coords[0]
    first = coords[1]

    zeroX = zero[0]
    zeroY = zero[1]
    firstX = first[0]
    firstY = first[1]

    cordi = ((zeroX, zeroY),(firstX, firstY))

    client = openrouteservice.Client(key = '5b3ce3597851110001cf62485adde571692449c2878c2cbe765e4fb5')
    geometry = client.directions(cordi) ['routes'][0]['geometry']

    decoded = convert.decode_polyline(geometry)

    print(decoded)

get_routes()
#dict = {'Python' : '.py', 'C++' : '.cpp', 'Java' : '.java'}

#json = json.dumps(dict)
#f = open("dict.json","w")
#f.write(json)
#f.close()
